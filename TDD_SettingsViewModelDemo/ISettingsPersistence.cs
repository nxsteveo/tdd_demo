﻿using System;
namespace TDD_SettingsViewModelDemo
{
    public interface ISettingsPersistence
    {
        object Get(string name);
        void Set(string name, object value);

        void Load();
        void Save();
    }
}
