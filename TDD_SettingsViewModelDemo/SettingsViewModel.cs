﻿﻿﻿﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace TDD_SettingsViewModelDemo
{
    class SettingsViewModel
    {
        public bool NotificationsEnabled { get; internal set; } = true;
        private readonly ISettingsPersistence _persistence;

        public SettingsViewModel(ISettingsPersistence persistence)
        {
            _persistence = persistence;   
        }

        public void Load()
        {
            _persistence.Load();

            NotificationsEnabled = (bool)_persistence.Get(nameof(NotificationsEnabled));
        }

        public void Save()
        {
            _persistence.Set(nameof(NotificationsEnabled), false);
            _persistence.Save();
        }

        public ICommand NotificationsCommand = new Command((o) =>
        {
            var vm = o as SettingsViewModel;
            vm.NotificationsEnabled = !vm.NotificationsEnabled;
        });
    }
}
