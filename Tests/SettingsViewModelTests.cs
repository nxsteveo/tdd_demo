﻿﻿﻿using System;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using TDD_SettingsViewModelDemo;

namespace Tests
{
    [TestFixture]
    public class SettingsViewModelTests
    {
        private SettingsViewModel ViewModel;
        private Mock<ISettingsPersistence> MockSettingsPersistence;

        [SetUp]
        public void SetUp()
        {
            MockSettingsPersistence = new Mock<ISettingsPersistence>();
            ViewModel = new SettingsViewModel(MockSettingsPersistence.Object);
        }

        [Test]
        public void NotificationsDefaultsToTrue()
        {
            ViewModel.NotificationsEnabled.Should().BeTrue();
        }

        [Test]
        public void SettingsAreLoadedFromPersistentStorage()
        {
            MakePersistence_NotificationsEnabled_ReturnFalse();

            ViewModel.Load();
            EnsureViewModelLoadsSettingsFromPersistentStorage();
        }

        [Test]
        public void SettingsAreSavedToPersistentStorage()
        {
            ViewModel.NotificationsEnabled = false;

            ViewModel.Save();

            MockSettingsPersistence.Verify(p => p.Set(nameof(ViewModel.NotificationsEnabled), false),
                                           Times.Once);
            MockSettingsPersistence.Verify(p => p.Save(), Times.Once);
        }

        [Test]
        public void NotificationsCommandTogglesProperty()
        {
            var initialState = ViewModel.NotificationsEnabled;

            ViewModel.NotificationsCommand.Execute(ViewModel);

            ViewModel.NotificationsEnabled.Should().Be(!initialState);
        }

        private void MakePersistence_NotificationsEnabled_ReturnFalse()
        {
            MockSettingsPersistence.Setup(p => p.Get(nameof(ViewModel.NotificationsEnabled)))
                                   .Returns(false);
        }

        private void EnsureViewModelLoadsSettingsFromPersistentStorage()
        {
            MockSettingsPersistence.Verify(p => p.Load(),
                                                       Times.Once,
                                                      "because the ViewModel.Load method was called");

            MockSettingsPersistence.Verify(p => p.Get(nameof(ViewModel.NotificationsEnabled)),
                                          Times.Once,
                                          "becasue the ViewModel should Get from persistence");

            ViewModel.NotificationsEnabled.Should().BeFalse();
        }
    }
}
